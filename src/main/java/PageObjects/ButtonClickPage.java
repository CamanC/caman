package PageObjects;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class ButtonClickPage {

    private WebDriver driver;

    private By webelementClickFromNewTab = By.cssSelector(".dropdown-toggle[id= 'button1']");
    private By succesMessage = By.cssSelector(".modal-body");
    private By closeBtnForWebElement = By.xpath("(//button[@data-dismiss=\"modal\"][@class=\"btn btn-default\"])[1]");
    private By buttonClicksF = By.id("button-clicks");
    private By closeBtnForJavaScriptElement = By.xpath("(//button[@data-dismiss=\"modal\"][@class=\"btn btn-default\"])[2]");
    private By succesMessageJavaScript = By.cssSelector(".modal-body");
    private By succesMessageForActionMove = By.cssSelector(".modal-body");
    private By closeBtnForAction = By.xpath("(//button[@data-dismiss=\"modal\"][@class=\"btn btn-default\"])[3]");


    public ButtonClickPage(WebDriver driver) {
        this.driver = driver;
    }

    public void clickWebElement() {
        driver.findElement(webelementClickFromNewTab).click();
    }

    public void javaScriptClick() {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("document.querySelector('#button2').click()");
    }

    public boolean successMessageText() {
        return driver.findElement(succesMessage).getText().contains("Well done");
    }


    public void clickOnBtnFild() {
        driver.findElement(buttonClicksF).click();
    }

    public void navogteToClickButtons() {

        Set<String> handle = driver.getWindowHandles();

        List<String> tabs = new ArrayList<>();
        for (Iterator<String> it = handle.iterator(); it.hasNext(); ) {
            tabs.add(it.next());
        }
        driver.switchTo().window(tabs.get(1));
    }

    public void closeTheMessage() {
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
        driver.findElement(closeBtnForWebElement).click();

    }


    public void successMessageTextForJava() {
        driver.findElement(succesMessageJavaScript).getText().contains("We can use JavaScript");
    }


    public void clickOnCloseBtnJavaScript() {
        List<WebElement> btnJavaScript = driver.findElements(closeBtnForJavaScriptElement);

        for (WebElement element : btnJavaScript) {
            element.click();
        }

    }

    public void clickOnActionMove() {

        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("document.querySelector('#button3').click()");

    }

    public void validateTextActionMove() {
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));

        driver.findElement(succesMessageForActionMove).getText().contains("Advanced user interactions");
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
    }

    public void closeActionMoveMessage() throws InterruptedException {
        Thread.sleep(300);

        Actions actions = new Actions(driver);
        WebElement bntForActionMove = driver.findElement(closeBtnForAction);
        actions.moveToElement(bntForActionMove).click().build().perform();
    }
}

