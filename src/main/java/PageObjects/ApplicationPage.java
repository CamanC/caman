package PageObjects;


import org.openqa.selenium.WebDriver;


public class ApplicationPage {
    public WebDriver driver;

    public ApplicationPage(WebDriver driver) {
        this.driver = driver;
    }

    public ContactPage navigatoToContactPage() {
        driver.get("https://webdriveruniversity.com/Contact-Us/contactus.html");
        return new ContactPage(driver);
    }


    //sa fac o metota unde fac click si se duce pe pagina contact
    //si sa asertez ca am ajuns pe pagina aceea (ceva cu get title, este mai usor)

    public ButtonClickPage mainPage() {
        driver.get("https://webdriveruniversity.com/");
        return new ButtonClickPage(driver);
    }

    public ActionsPage pageMain(){
        driver.get("https://webdriveruniversity.com/");
        return new ActionsPage(driver);

    }


}
