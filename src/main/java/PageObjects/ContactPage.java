package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class ContactPage {
    private WebDriver driver;

    private final By firstNameField = By.name("first_name");
    private final By lastNameField = By.name("last_name");
    private final By emailField = By.name("email");
    private final By commentsField = By.name("message");
    public By submitButton = By.cssSelector(".contact_button[type='submit']");
    public By successMessage = By.cssSelector(".cbp-af-inner #contact_reply");
    private By messageForIncorectSubmit = By.xpath("//*[br]");

    public ContactPage(WebDriver driver) {
        this.driver = driver;
    }

    public void fillFirstName(String firstName) {
        driver.findElement(firstNameField).sendKeys(firstName);
    }

    public void fillLastName(String lastName) {
        driver.findElement(lastNameField).sendKeys(lastName);
    }

    public void fillEmail(String email) {
        driver.findElement(emailField).sendKeys(email);
    }

    public void fillComments(String comments) {
        driver.findElement(commentsField).sendKeys(comments);
    }

    public void pressSubmitBtn() {
        driver.findElement(submitButton).click();
    }

    public String getSuccessMessage() {
        return driver.findElement(successMessage).getText();
    }


    public void messageForInvalidSubmit() {
        driver.findElement(messageForIncorectSubmit).getText().contains("all fields are required");

    }

    public void setMessageForInvalidMessage() {
        driver.findElement(messageForIncorectSubmit).getText().contains("Invalid email address");
    }

}
