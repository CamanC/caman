package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class ActionsPage {

    private WebDriver driver;

    private By doubleClickArea = By.id("double-click");
    private By actionArea = By.xpath("//*[h1='ACTIONS']");
    private By succesfulDoubleClick = By.xpath("//*[@class='div-double-click double']");
    private By clickAndHoldSection = By.xpath("//*[@class='col-lg-12 text-center'][@id='click-box']");


    public ActionsPage(WebDriver driver) {
        this.driver = driver;
    }


    public void clickOnTheActionArea() {

        Actions actions = new Actions(driver);
        WebElement actionArea1 = driver.findElement(actionArea);
        actions.moveToElement(actionArea1).click().build().perform();
    }

    public void navigteToActionPage() {

        Set<String> handle = driver.getWindowHandles();

        List<String> tabs = new ArrayList<>();
        for (Iterator<String> it = handle.iterator(); it.hasNext(); ) {
            tabs.add(it.next());
        }
        driver.switchTo().window(tabs.get(1));
    }

    public void dragAndDropObject() {
        WebElement objectToBeDroped = driver.findElement(By.xpath("//*[@class='ui-widget-content ui-draggable ui-draggable-handle']"));
        WebElement dropHereSectionn = driver.findElement(By.xpath("//*[@class='ui-widget-header ui-droppable']"));

        Actions action = new Actions(driver);
        action.dragAndDrop(objectToBeDroped, dropHereSectionn).build().perform();
    }

    public void validateSuccessDrop() {

        if (driver.findElement(By.xpath("//*[b='Dropped!']")).isDisplayed()) {
            System.out.println("Perfect Displayed !!!");
        } else {

            System.out.println("Perfect not Displayed !!!");
        }
    }

    public void makeAdoubleClick() {
        Actions actions = new Actions(driver);
        WebElement doubleClickSection = driver.findElement(doubleClickArea);
        actions.doubleClick(doubleClickSection).build().perform();
    }

    public void succesDoubleClick() {
        driver.findElement(succesfulDoubleClick).equals(succesfulDoubleClick);

    }

    public void clickAndHold() {
        Actions actions = new Actions(driver);
        WebElement clickable = driver.findElement(clickAndHoldSection);
        actions.clickAndHold(clickable).build().perform();
    }

    public void validateClickAndHold(){
        driver.findElement(clickAndHoldSection).getText().contains("Well done!");

    }

    public void validateReleaseClick(){
        driver.findElement(clickAndHoldSection).getText().contains("Dont release me!!!");

    }

    public void refreshPage (){
        driver.navigate().refresh();
    }

    public void validateRefresh(){
        driver.findElement(clickAndHoldSection).getText().contains("Click and Hold!");

    }




}

