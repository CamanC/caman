package stepdefinitions;

import PageObjects.ActionsPage;
import PageObjects.ApplicationPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static stepdefinitions.Hooks.driver;


public class ActionPageSteps {

    ActionsPage actionsPage;

    @Given("Open first page")
    public void openFirstPage() {
        actionsPage = new ApplicationPage(driver).pageMain();
    }

    @When("Click on the actions area")
    public void clickOnTheActionArea(){
        actionsPage.clickOnTheActionArea();

    }

    @And("Navigate to the action page")
    public void navigateToTheActionPage() {
        actionsPage.navigteToActionPage();
    }

    @And("Drag and drop the object on the specific area")
    public void dragAndDropTheObjectOnTheSpecificArea() {
        actionsPage.dragAndDropObject();

    }

    @Then("Validate if object was dropped")
    public void validateIfObjectWasDropped() {
        actionsPage.validateSuccessDrop();

    }

    @When("Make a double click on the specific area")
    public void makeADoubleClickOnTheSpecificArea() {
        actionsPage.makeAdoubleClick();
    }

    @Then("Validate if double ckick was made")
    public void validateIfDoubleCkickWasMade() {
        actionsPage.succesDoubleClick();
    }


    @When("Press and hold click")
    public void pressAndHoldClick() {
        actionsPage.clickAndHold();
    }

    @And("Validate the text for hold click")
    public void validateTheTextForHoldClick() {
        actionsPage.validateClickAndHold();
    }

    @And("Release click")
    public void releaseClick() {
        actionsPage.validateReleaseClick();
    }

    @And("Refresh the action page")
    public void refreshTheActionPage() {
        actionsPage.refreshPage();
    }

    @Then("Validate if refresh was made")
    public void validateIfRefreshWasMade() {
        actionsPage.validateRefresh();
    }
}
