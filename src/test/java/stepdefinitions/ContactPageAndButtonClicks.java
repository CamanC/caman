package stepdefinitions;

import PageObjects.ApplicationPage;
import PageObjects.ButtonClickPage;
import PageObjects.ContactPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;


import static stepdefinitions.Hooks.driver;

public class ContactPageAndButtonClicks {
    ContactPage contactPage;
    ButtonClickPage buttonClickPage;

    @And("Navigate to the contact page")
    public void navigateToTheContactPage() {
        contactPage = new ApplicationPage(driver).navigatoToContactPage();

    }

    @When("Fill the first name field {string}")
    public void fillTheFirstNameField(String name) {
        contactPage.fillFirstName(name);
    }

    @And("Fill the name field  {string}")
    public void fillTheNameField(String lastName) {
        contactPage.fillLastName(lastName);

    }

    @And("Fill the email field {string}")
    public void fillTheEmailfield(String email) {
        contactPage.fillEmail(email);

    }

    @And("Fill the comments field {string}")
    public void fillTheCommentsField(String comments) {
        contactPage.fillComments(comments);
    }


    @And("Click on submit button")
    public void clickOnSubmitButton() {
       contactPage.pressSubmitBtn();
    }

    @Then("Verify the text for succesfully sent message")
    public void verifyTheTextForSuccesfullySentMessage() {
        Assert.assertEquals("Thank You for your Message!", contactPage.getSuccessMessage());
    }

    @Then("Verify the text after submit without comments")
    public void verifyTheTextAfterSubmitWithoutComments() {
        contactPage.messageForInvalidSubmit();
    }

    @Then("Verify the text after submit without email")
    public void verifyTheTextAfterSubmitWithoutEmail() {
        contactPage.setMessageForInvalidMessage();
    }

    //========================Button clicks steps=======================================

    @Given("Open main page")
    public void openMainPage() {
        buttonClickPage = new ApplicationPage(driver).mainPage();
    }


    @When("Click on the Button click field")
    public void clickOnTheButtonClickField() {
        buttonClickPage.clickOnBtnFild();
    }

    @And("Click on Webelement click")
    public void clickOnWebelementClick() {
        buttonClickPage.clickWebElement();
    }

    @And("Move to click buttons tab")
    public void moveToClickButtonsTab() {
        buttonClickPage.navogteToClickButtons();
    }

    @And("Validate the text after click")
    public void validateTheTextAfterClick() {
        buttonClickPage.successMessageText();
    }

    @Then("Close the message")
    public void closeTheMessage() {
       buttonClickPage.closeTheMessage();
    }

    @When("Click on the JavaScript click")
    public void clickOnTheJavaScriptClick() {
        buttonClickPage.javaScriptClick();
    }

    @And("Validate the text after new click")
    public void validateTheTextAfterNewClick() {
    buttonClickPage.successMessageTextForJava();
    }

    @Then("Close the new message")
    public void closeTheNewMessage(){
        buttonClickPage.clickOnCloseBtnJavaScript();
    }

    @When("Click on the Action Move Click")
    public void clickOnTheActionMoveClick() {
        buttonClickPage.clickOnActionMove();
    }

    @And("Validate the text")
    public void validateTheText() {
        buttonClickPage.validateTextActionMove();
    }

    @Then("Close the last message")
    public void closeTheLastMessage() throws InterruptedException {
        buttonClickPage.closeActionMoveMessage();

    }



}