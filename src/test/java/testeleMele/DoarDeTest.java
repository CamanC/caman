package testeleMele;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.BaseTestClass;

public class DoarDeTest extends BaseTestClass {
    //aceasta pagina nu respecta conventiile,
    //este facuta doar pentru a testa diferite chestii

    @Test
    public void navigatoToContactPage (){
        driver.get("https://webdriveruniversity.com");

        WebElement submitButton= driver.findElement(By.cssSelector("#contact-us"));
        submitButton.click();


    }

    @Test
    public void submitButon(){
        driver.get("https://webdriveruniversity.com/Contact-Us/contactus.html");
        WebElement submitBtn = driver.findElement(By.tagName("SUBMIT"));
        submitBtn.click();


    }

    @Test
    public void successMessage (){
        driver.get("https://webdriveruniversity.com/Contact-Us/contactus.html");

       // WebElement contactBtn= driver.findElement(By.cssSelector("#contact-us"));
       // contactBtn.click();


        WebElement firstNameField = driver.findElement(By.name("first_name"));
        firstNameField.sendKeys("asd");

        WebElement lastNameField = driver.findElement( By.name("last_name"));
        lastNameField.sendKeys("asd");

        WebElement emailField =  driver.findElement(By.name("email"));
        emailField.sendKeys("nicu@yahoo.com");

        WebElement commentsField = driver.findElement(By.name("message"));
        commentsField.sendKeys("comment");

        WebElement submit = driver.findElement(By.cssSelector(".contact_button[type='submit']"));
        submit.click();

        WebElement succesMessage = driver.findElement(By.cssSelector(".cbp-af-inner #contact_reply"));
        Assert.assertEquals("Thank You for your Message!",succesMessage.getText() );


    }

    @Test
    public void testFirstPage (){

        driver.get("https://webdriveruniversity.com/Click-Buttons/index.html");
        WebElement contactPageBtn = driver.findElement(By.id("button1"));
        contactPageBtn.click();
    }

    @Test
    public void verifySuccesMessage(){
        driver.get("https://webdriveruniversity.com/Click-Buttons/index.html");
        WebElement clickBtn = driver.findElement(By.id("button1"));
        clickBtn.click();
        WebElement message = driver.findElement(By.cssSelector(".modal-body"));
        message.getText();

    }


}
