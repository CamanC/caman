Feature: Testing Contact Page

  Scenario Outline: First scenario with succes submit
    Given Open main page
    And Navigate to the contact page
    When Fill the first name field "<name>"
    And Fill the name field  "<lastName>"
    And Fill the email field "<email>"
    And Fill the comments field "<comments>"
    And  Click on submit button
    Then Verify the text for succesfully sent message


    Examples:
      | name | lastName | email          | comments     |
      | nume1  | nume2     | nume@test.com  | test123123   |

  Scenario Outline: Open contact page and fill with first name, last name and email fields
    Given Open main page
    And Navigate to the contact page
    When Fill the first name field "<name>"
    And Fill the name field  "<lastName>"
    And Fill the email field "<email>"
    And Click on submit button
    Then Verify the text after submit without comments


    Examples:
      | name | lastName          | email          |
      | nume  | celalat nume     | nume@test.com  |

  Scenario Outline: Open contact page and fill with first name, last name and comments fields
      Given Open main page
      And Navigate to the contact page
      When Fill the first name field "<name>"
      And Fill the name field  "<lastName>"
      And Fill the comments field "<comments>"
      And Click on submit button
      Then Verify the text after submit without email

      Examples:
        | name | lastName |  comments                      |
        | ion  | ion1     |  testez doar, nu va speriati   |