Feature: Testing button click

  Scenario: Click on button clicks and validate the message
    Given Open main page
    When Click on the Button click field
    And Move to click buttons tab
    And Click on Webelement click
    And Validate the text after click
    Then Close the message

    When Click on the JavaScript click
    And Validate the text after new click
    Then Close the new message

    When Click on the Action Move Click
    And Validate the text
    Then Close the last message