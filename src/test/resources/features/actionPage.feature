Feature: Testing actions page


Scenario: Testing all the functionality from the action page
  Given Open first page
  #drag and drop the object
  When Click on the actions area
  And Navigate to the action page
  And Drag and drop the object on the specific area
  Then Validate if object was dropped

  #make a double click
  When Make a double click on the specific area
  Then Validate if double ckick was made

  #click and hold
  When Press and hold click
  And Validate the text for hold click
  And Release click
  And Refresh the action page
  Then Validate if refresh was made
  #aici fac validarea refreshului cu ajutorul textului de la click and hold








